"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.useLeafletIsZooming = void 0;

var _onLeafletInput = require("./onLeafletInput");

var useLeafletIsZooming = function useLeafletIsZooming() {
    return (0, _onLeafletInput.useLeafletInput)("zoomstart", "zoomend");
};

exports.useLeafletIsZooming = useLeafletIsZooming;