"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.useLeafletZoom = void 0;

var _onLeafletEvent = require("./onLeafletEvent");

var useLeafletZoom = function useLeafletZoom() {
    return (0, _onLeafletEvent.useLeafletData)(getMapZoom, "zoomend");
};

exports.useLeafletZoom = useLeafletZoom;

var getMapZoom = function getMapZoom(map) {
    return map && map.getZoom() || 0;
};