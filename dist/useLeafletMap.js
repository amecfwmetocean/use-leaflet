"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.useLeafletMap = void 0;

var _reactLeaflet = require("react-leaflet");

var useLeafletMap = function useLeafletMap() {
    return (0, _reactLeaflet.useLeaflet)().map;
};
/**
 * @external Map
 * @see leaflet {@link https://leafletjs.com/reference.html#map Map} type.
 */


exports.useLeafletMap = useLeafletMap;