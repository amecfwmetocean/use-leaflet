"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.useLeafletData = void 0;

var _react = require("react");

var _useLeafletMap = require("./useLeafletMap");

var useLeafletData = function useLeafletData(getLeafletData, event) {
    var map = (0, _useLeafletMap.useLeafletMap)();

    var _ref = (0, _react.useState)(function () {
        return getLeafletData(map);
    }),
        data = _ref[0],
        setData = _ref[1];

    (0, _react.useEffect)(function () {
        var effectMounted = true;

        var doCallback = function doCallback() {
            return effectMounted && setData(getLeafletData(map));
        };

        setImmediate(doCallback);
        if (map) map.on(event, doCallback);
        return function () {
            effectMounted = false;
            if (map) map.off(event, doCallback);
        };
    }, [event, getLeafletData, map]);
    return data;
};

exports.useLeafletData = useLeafletData;