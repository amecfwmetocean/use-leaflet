"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.useLeafletIsMoving = void 0;

var _onLeafletInput = require("./onLeafletInput");

var useLeafletIsMoving = function useLeafletIsMoving() {
    return (0, _onLeafletInput.useLeafletInput)("movestart", "moveend");
};

exports.useLeafletIsMoving = useLeafletIsMoving;