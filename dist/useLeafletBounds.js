"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.useLeafletBounds = void 0;

var _onLeafletEvent = require("./onLeafletEvent");

var useLeafletBounds = function useLeafletBounds() {
    return (0, _onLeafletEvent.useLeafletData)(getMapBounds, "moveend");
};

exports.useLeafletBounds = useLeafletBounds;

var getMapBounds = function getMapBounds(map) {
    if (!map) return [[Infinity, Infinity], [-Infinity, -Infinity]];
    var bounds = map.getBounds();
    return [[bounds.getSouth(), bounds.getWest()], [bounds.getNorth(), bounds.getEast()]];
};