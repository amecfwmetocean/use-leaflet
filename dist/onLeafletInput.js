"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.useLeafletInput = void 0;

var _react = require("react");

var _useLeafletMap = require("./useLeafletMap");

var useLeafletInput = function useLeafletInput(inputStartEvent, inputStopEvent, onGetInitialState) {
    if (onGetInitialState === void 0) {
        onGetInitialState = getFalse;
    }

    var map = (0, _useLeafletMap.useLeafletMap)();

    var _ref = (0, _react.useState)(onGetInitialState(map)),
        isInput = _ref[0],
        setIsInput = _ref[1];

    (0, _react.useEffect)(function () {
        var effectMounted = true;

        var handleStart = function handleStart() {
            return effectMounted && setIsInput(true);
        };

        var handleEnd = function handleEnd() {
            return effectMounted && setIsInput(false);
        };

        if (map) {
            map.on(inputStartEvent, handleStart);
            map.on(inputStopEvent, handleEnd);
        }

        return function () {
            effectMounted = false;

            if (map) {
                map.off(inputStartEvent, handleStart);
                map.off(inputStopEvent, handleEnd);
            }
        };
    }, [inputStartEvent, inputStopEvent, map]);
    return isInput;
};

exports.useLeafletInput = useLeafletInput;

var getFalse = function getFalse() {
    return false;
};