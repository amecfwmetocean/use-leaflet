"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.useLeafletCenter = void 0;

var _onLeafletEvent = require("./onLeafletEvent");

var useLeafletCenter = function useLeafletCenter() {
    return (0, _onLeafletEvent.useLeafletData)(getMapCenter, "moveend");
};

exports.useLeafletCenter = useLeafletCenter;

var getMapCenter = function getMapCenter(map) {
    if (!map) return [0, 0];

    var _map$getCenter = map.getCenter(),
        lat = _map$getCenter.lat,
        lng = _map$getCenter.lng;

    return [lat, lng];
};